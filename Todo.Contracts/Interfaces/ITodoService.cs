﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Todo.Contracts.Data;

namespace Todo.Contracts.Interfaces
{
    public interface ITodoService
    {
		TodoTask Add(string taskText);
		TodoTask Edit(TodoTask task);

		void Toggle(Guid taskKey);
		void Remove(Guid taskKey);

		IEnumerable<TodoTask> GetTasks();
	}
}
