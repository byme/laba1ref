﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Contracts.Data
{
	public class TodoTask
	{
		public bool Finished { get; set; }
		public Guid? Key { get; set; }
		public string Text { get; set; }
	}
}
