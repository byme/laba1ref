﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Todo.AspNet.View.ViewModels;
using Todo.Contracts.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Todo.Controllers
{
	[Route("todo")]
	public class TodoController : Controller
	{
		const string ShowFinishedCookies = "ShowFinished";
		private ITodoService TodoService;

		public TodoController(ITodoService todoService)
		{
			this.TodoService = todoService;
		}

		[Route("mylist")]
		public IActionResult Index()
		{
			if (this.Request.Cookies.ContainsKey(ShowFinishedCookies))
				return this.View(new TaskListModel
				{
					ShowFinished = true,
					Tasks = this.TodoService.GetTasks()
				});

			return this.View(
				new TaskListModel
				{
					ShowFinished = false,
					Tasks = this.TodoService.GetTasks().Where(o => !o.Finished)
				});
		}

		[Route("toggle")]
		public IActionResult ToggleTask(Guid key)
		{
			this.TodoService.Toggle(key);
			return this.RedirectToAction(nameof(this.Index));
		}

		[Route("add")]
		public IActionResult AddTask([FromForm]string taskText)
		{
			this.TodoService.Add(taskText);
			return this.RedirectToAction(nameof(this.Index));
		}


		[Route("remove/{id:guid}")]
		public IActionResult RemoveTask(Guid id)
		{
			this.TodoService.Remove(id);
			return this.RedirectToAction(nameof(this.Index));
		}

		[Route("togglefinihsedtasks")]
		public IActionResult ToggleVisibilityFinishedTasks()
		{
			if (this.Request.Cookies.ContainsKey(ShowFinishedCookies))
				this.Response.Cookies.Delete(ShowFinishedCookies);
			else
				this.Response.Cookies.Append(ShowFinishedCookies, string.Empty);

			return this.RedirectToAction(nameof(this.Index));
		}


	}
}
