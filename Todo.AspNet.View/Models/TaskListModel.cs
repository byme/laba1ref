﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Contracts.Data;

namespace Todo.AspNet.View.ViewModels
{
	public class TaskListModel
	{
		public IEnumerable<TodoTask> Tasks
		{
			get;
			set;
		}

		public bool ShowFinished
		{
			get;
			set;
		}
	}
}
