﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Todo.Contracts.Data;
using Todo.Contracts.Interfaces;

namespace Todo.Business.Services
{
	public class TodoService : ITodoService
	{
		private static Dictionary<Guid, TodoTask> Tasks;

		static TodoService()
		{
			Tasks = Enumerable.Range(1, 10).Select(o => new TodoTask
			{
				Key = Guid.NewGuid(),
				Text = "Test" + o,
				Finished = o % 2 == 0
			}).
			ToDictionary(o => o.Key.Value);
		}

		public TodoTask Add(string taskText)
		{
			var task = new TodoTask
			{
				Key = Guid.NewGuid(),
				Text = taskText,
				Finished = false
			};

			Tasks.Add(task.Key.Value, task);
			return task;
		}

		public TodoTask Edit(TodoTask task)
		{
			if (task?.Key is null)
				throw new InvalidOperationException("Task should have key");

			return Tasks[task.Key.Value] = task;
		}

		public void Toggle(Guid taskKey)
		{
			Tasks.TryGetValue(taskKey, out TodoTask task);
			if (task is null)
				throw new InvalidOperationException($"Task with key {task} is missing");

			task.Finished = !task.Finished;
		}

		public IEnumerable<TodoTask> GetTasks()
		{
			return Tasks.Values.ToArray();
		}

		public void Remove(Guid taskKey)
		{
			if (!Tasks.Remove(taskKey))
				throw new InvalidOperationException($"Task with key {taskKey} is missing");
		}
	}
}
