﻿

var http = function () {
    var scope = new Object();

    scope.get = function (url, success = null, error = null) {
        var request = new XMLHttpRequest();
        request.open('GET', url);
        request.send(null);
        request.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;
            if (request.readyState === DONE) {
                if (request.status === OK)
                    if (request.responseText) {
                        success(JSON.parse(request.responseText));
                    } else {
                        success(null);
                    }
            } else {
                if (error) {
                    error(request.status);
                }
            }
        }
    };

    return scope;
} ();

var TodoService = function () {
    var scope = new Object();
    var tasks = null;
    var finishedTasksVisible = true;
    var add = function (message) {
        http.get("http://localhost:5001/api/todo/add/" + message, function (responce) {
            scope.get();
        });
    };

    var setupTasks = function (responce) {
        var todolist = document.getElementById("todolist");
        var todolistHtml = "";
        var docfrag = document.createDocumentFragment();

        for (var i = 0; i < responce.length; i++) {
            var task = responce[i];

            if (!finishedTasksVisible && task.finished)
                continue;
            var tr = document.createElement("tr");
            var td = document.createElement("td");
            var div = document.createElement("div");
            tr.appendChild(td);
            td.appendChild(div);

            div.className  = "ui checkbox";

            var input = document.createElement("input");
            input.type = "checkbox";

            if (task.finished) {
                input.checked = "";
            }

            input.onclick = function () { scope.toggle(task.key); }

            var label = document.createElement("label");
            label.innerText = task.text;

            div.appendChild(input);
            div.appendChild(label);

            var td2 = document.createElement("td");
            tr.appendChild(td2);
            td2.className = "right aligned";
            var link = document.createElement("a");
            td2.appendChild(link);

            link.onclick = function () {
                var key = responce[i].key
                scope.remove(key);
            }

            var icon = document.createElement("i");
            link.appendChild(icon);
            icon.className = "red remove icon";
            icon.innerText = "";

            docfrag.appendChild(tr);
        }
        todolist.innerHTML = null;
        todolist.appendChild(docfrag);
    }

    scope.tryAdd = function () {
        var taskText = document.getElementById("taskText");
        if (taskText.value) {
            add(taskText.value);
            taskText.value = "";
        }
    }

    scope.remove = function (key) {
        http.get("http://localhost:5001/api/todo/remove/" + key, function (responce) {
            scope.get();
        });
    };

    scope.toggle = function (key) {
        http.get("http://localhost:5001/api/todo/task/" + key + "/toggle", function (responce) {
            scope.get();
        });
    };

    scope.get = function () {
        http.get("http://localhost:5001/api/todo/tasks", function (responce) {
            tasks = responce;
            setupTasks(tasks);
        });
    };

    scope.toggleTaskVisibility = function () {
        finishedTasksVisible = !finishedTasksVisible;
        var taskButton = document.getElementById("taskButton");
        if (finishedTasksVisible) {
            taskButton.innerText = "Hide finished tasks";
        }
        else {
            taskButton.innerText = "Show finished tasks";
        }


        setupTasks(tasks);
    };

    return scope;
} ();


window.onload = function (a) {
    TodoService.get();
    TodoService.toggleTaskVisibility();
};
