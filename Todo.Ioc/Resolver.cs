﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Todo.Business.Services;
using Todo.Contracts.Interfaces;

namespace Todo.Ioc
{
    public static class Resolver
    {
		public static void AddTodoServices(this IServiceCollection serviceCollection)
		{
			serviceCollection.AddTransient<ITodoService, TodoService>();
		}
	}
}
