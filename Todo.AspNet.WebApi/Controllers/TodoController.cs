﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Todo.Contracts.Data;
using Todo.Contracts.Interfaces;

namespace Todo.AspNet.WebApi.Controllers
{
    [Route("api/todo")]
	public class TodoController : Controller
    {
		private ITodoService TodoService;

		public TodoController(ITodoService todoService)
		{
			this.TodoService = todoService;
		}

		[Route("tasks")]
		public IActionResult GetTasks()
		{
			return this.Ok(this.TodoService.GetTasks());
		}

		[Route("task/{key:guid}/toggle")]
		public IActionResult ToggleTask(Guid key)
		{
			this.TodoService.Toggle(key);
			return this.Ok();
		}

		[Route("remove/{key:guid}")]
		public IActionResult RemoveTask(Guid key)
		{
			this.TodoService.Remove(key);
			return this.Ok();
		}

		[Route("add/{message}")]
		public IActionResult StartTask(string message)
		{
			this.TodoService.Add(message);
			return this.Ok();
		}
    }
}
